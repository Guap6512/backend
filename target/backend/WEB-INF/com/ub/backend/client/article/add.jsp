<section>
    <div class="container">
        <%--Header--%>
        <div class="row">
            <div class="col-md-8 offset-md-2 article-add-title">
                <h1 class="article-add-title-h1">Добавление статьи</h1>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8 offset-md-2 article-add-form">
                <div class="article-add-form-group">
                    <label class="article-add-form-group-label">
                        Заголовок
                    </label>
                    <input class="article-add-form-group-input"/>
                    <label class="article-add-form-group-label">
                        Описание
                    </label>
                    <input class="article-add-form-group-input mod-textarea"/>
                </div>
            </div>
        </div>

        <%--Footer--%>
        <div class="row">
            <div class="col-md-4 offset-md-2">
                <div class="article-add-form-footer">
                    <label class="article-add-form-group-label">
                        Прикрепить изображение
                    </label>
                    <input type="file" class="article-add-form-group-input mod-file" id="file"/>
                    <label class="article-add-form-group-label-file" for="file"></label>
                </div>
            </div>
            <div class="col-md-4 article-add-form-footer-btns">
                <a href="/" class="article-add-form-footer-btns-cancel">Отмена</a>
                <button type="submit" class="article-add-form-footer-submit">
                    Добавить
                </button>
            </div>
        </div>
    </div>
</section>