<%@ page import="com.ub.backend.article.routes.ArticleClientRoute" %>
<section>
    <div class="container header">
        <div class="row">
            <div class="col-md-6">
                <div class="header-logo">
                    <img src="/static/backend/img/logo.png" class="header-logo-img"/>
                </div>
                <div class="header-title">Статьи, 24</div>
            </div>
            <div class="col-md-6">
                <div class="header-article-new">
                    <a href="<%= ArticleClientRoute.ADD%>" class="header-article-new-link">
                        <img class="header-article-new-link-img" src="/static/backend/img/article-new.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article-list">
        <div class="row">
            <div class="col-md-6">
                <div class="article-list-item">
                    <a href="/article/view" class="article-list-item-link">
                        <div class="article-list-item-pic"
                        style="background-image: url('/static/backend/img/article-pic.jpg')"></div>
                        <div class="article-list-item-title">
                            Что нового в IOS 10
                        </div>
                        <div class="article-list-item-date">
                            11 янв, 2017
                        </div>
                        <div class="article-list-item-preview">
                            Самая последняя версия операционной системы для продукции компании Apple iOS 10 поставляется со встроенными функциями для корпоративных клиентов Cisco. По словам разработчиков это позволит компаниям…
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="article-list-item">
                    <a href="#" class="article-list-item-link">
                        <div class="article-list-item-pic"
                             style="background-image: url('/static/backend/img/article-pic.jpg')"></div>
                        <div class="article-list-item-title">
                            Мобильные приложения в маркетинге
                        </div>
                        <div class="article-list-item-date">
                            11 янв, 2017
                        </div>
                        <div class="article-list-item-preview">
                            Мобильные приложения - мощный инструмент маркетинга. И многочисленные примеры компаний, ведущих бизнес в различных сферах, подтверждают это.
                            Мобильные приложения - мощный инструмент маркетинга.
                            И многочисленные примеры компаний, ведущих бизнес в различных сферах, подтверждают это.
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="article-list-item">
                    <a href="#" class="article-list-item-link">
                        <div class="article-list-item-pic"
                             style="background-image: url('/static/backend/img/article-pic.jpg')"></div>
                        <div class="article-list-item-title">
                            Мобильные приложения в маркетинге
                        </div>
                        <div class="article-list-item-date">
                            11 янв, 2017
                        </div>
                        <div class="article-list-item-preview">
                            Мобильные приложения - мощный инструмент маркетинга. И многочисленные примеры компаний, ведущих бизнес в различных сферах, подтверждают это.
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="article-list-item">
                    <a href="#" class="article-list-item-link">
                        <div class="article-list-item-pic"
                             style="background-image: url('/static/backend/img/article-pic.jpg')"></div>
                        <div class="article-list-item-title">
                            Мобильные приложения в маркетинге
                        </div>
                        <div class="article-list-item-date">
                            11 янв, 2017
                        </div>
                        <div class="article-list-item-preview">
                            Мобильные приложения - мощный инструмент маркетинга.
                            И многочисленные примеры компаний, ведущих бизнес в различных сферах, подтверждают это.
                            Мобильные приложения - мощный инструмент маркетинга.
                            И многочисленные примеры компаний, ведущих бизнес в различных сферах, подтверждают это.
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>