<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 article-view-content">
                <div class="article-view-content-header">
                    <h1 class="article-view-content-header-h1">Что нового в IOS 10</h1>
                    <img src="/static/backend/img/article-big-picture.png" class="article-view-content-image">
                    <div class="article-view-content-text">
                        Сегодня Крейг Федериги, вице-президент Apple, представил «самое большое» обновление (по его словам) iOS. Новая версия мобильной операционной системы Apple включает совершенно новый экран блокировки, обновленные уведомления, расширенную поддержку 3D Touch и новые инструменты для взаимодействия с приложениями. Заинтригованы? Давайте подробнее.

                        Вот как выглядит обновленный экран блокировки. Появилась возможность совершать действия с уведомлениями прямо на данном экране при помощи 3D Touch, кнопка «очистить все уведомления» и другие возможности.

                        Удивительные изменения претерпел поиск Spotlight. Здесь теперь не только предложения Siri и результаты поиска, но и другая полезная информация: результаты матчей, ваш календарь, погода и даже воспроизведение видео!
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="article-view-comments">
            <div class="article-view-comments-form">
                <div class="row">
                    <div class="col-md-1 offset-md-2">
                        <img src="/static/backend/img/avatar-user.png">
                    </div>
                    <div class="col-md-6">
                        <input class="article-view-comments-form-input">
                    </div>
                    <div class="col-md-1">
                        <input type="image" src="/static/backend/img/send-comment.png" class="article-view-comments-form-button">
                    </div>
                </div>
            </div>
            <div class="article-view-comments-list">
                <div class="article-view-comments-list-item">
                    <div class="row">
                        <div class="col-md-1 offset-md-2 article-view-comments-avatar">
                            <img src="/static/backend/img/avatar-1.png"/>
                        </div>
                        <div class="col-md-7">
                            <div class="article-view-comments-list-item-username">
                                ZelektrickDOG
                            </div>
                            <div class="article-view-comments-list-item-date">
                                13 июня 2016
                            </div>
                            <div class="article-view-comments-list-item-text">
                                А вообще на андроид похоже
                            </div>
                        </div>
                    </div>
                </div>

                <div class="article-view-comments-list-item">
                    <div class="row">
                        <div class="col-md-1 offset-md-2 article-view-comments-avatar">
                            <img src="/static/backend/img/avatar-2.png"/>
                        </div>
                        <div class="col-md-7">
                            <div class="article-view-comments-list-item-username">
                                Den4ik7755
                            </div>
                            <div class="article-view-comments-list-item-date">
                                13 июня 2016
                            </div>
                            <div class="article-view-comments-list-item-text">
                                Какие устроиройства будут поддерживать обновление???
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>