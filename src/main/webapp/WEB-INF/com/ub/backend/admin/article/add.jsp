<%@ page import="com.ub.backend.article.routes.ArticleAdminRoutes" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="section">
    <form:form action="<%= ArticleAdminRoutes.ADD%>" cssClass="card-panel"
               modelAttribute="doc" method="post" enctype="multipart/form-data">

        <h4 class="header2"><s:message code="ubcore.admin.form.adding"/></h4>

        <c:set value="${doc}" var="doc" scope="request"/>

        <div class="row">
            <div class="input-field col s12">
                <form:input path="title" id="title"/>
                <label for="title">Заголовок</label>

            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:input path="description" id="title"/>
                <label for="description">Описание</label>

            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <input type="file" id="file" name="pic" class="">
                <label for="pic">Изображение</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit">
                    <s:message code="ubcore.admin.form.submit"/>
                    <i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>