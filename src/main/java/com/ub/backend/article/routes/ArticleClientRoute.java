package com.ub.backend.article.routes;

public class ArticleClientRoute {
    public static final String ROOT = "/article";

    public static final String ADD = ROOT + "/add";

    public static final String SUCCESS = ROOT + "/success";

    public static final String VIEW = ROOT + "/view";
}
