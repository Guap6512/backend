package com.ub.backend.base.controller;

import com.ub.backend.base.service.PictureApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseApiController {
    @Autowired private PictureApiService pictureApiService;

//    @PostMapping(value = BaseApiRoutes.PICTURE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public FileResponse uploadPicture(
//            @RequestParam MultipartFile file_content,
//            @RequestParam(required = false) String field_name
//    ) throws BadRequestApiException {
//
//        return pictureApiService.uploadPicture(file_content, field_name);
//    }
//
//    @DeleteMapping(value = BaseApiRoutes.PICTURE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public ApiResponse<String> removePicture(@RequestParam ObjectId pictureId){
//
//        return new ApiResponse<>(pictureApiService.removePicture(pictureId));
//    }
}
