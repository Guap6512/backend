package documentation.v1;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
@ContextConfiguration("/mvc-dispatcher-servlet.xml")
public class BaseDocumentation {
    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    MockMvc mockMvc;
    @Autowired protected WebApplicationContext context;

    ObjectId defaultId = ObjectId.createFromLegacyFormat(0, 0, 0);

    @Autowired protected MongoTemplate mongoTemplate;

    void init(){

    }

    void destroy(){

    }

    @Before
    public void onInit(){
        MockMvcRestDocumentationConfigurer configurer =
                MockMvcRestDocumentation.documentationConfiguration(restDocumentation);
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(configurer)
                .build();
    }

    @After
    public void onDestroy(){}

}
